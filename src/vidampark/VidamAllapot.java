package vidampark;

import allapotter.Allapot;
import allapotter.HibasOperatorException;
import allapotter.Operator;

import java.util.HashSet;

/**
 * Created by Erubo on 17/05/15.
 */
public class VidamAllapot extends Allapot {

    static{
        operatorok = new HashSet<>();
        final int[] honapok = {3,4,7,8,11};
        for (int gy = 1; gy <= 5; gy++) {
            for (int j = 1; j <= 5; j++) {
                operatorok.add(new Honap(gy, honapok[j-1]));
                operatorok.add(new Jatek(gy, j));
                operatorok.add(new Torta(gy, j));
            }
        }
    }

    private int[][] h;

    public VidamAllapot() {
        h = new int[6][4];
    }

    private VidamAllapot(VidamAllapot szulo) {
        h = new int[6][4];
        for (int i = 0; i < 6 ; i++) {
            for (int j = 0; j < 4; j++) {
                h[i][j] = szulo.h[i][j];
            }
        }
    }

    @Override
    public boolean celAllapot() {
        return h[5][3] != 0;
    }

    @Override
    public boolean elofeltetel(Operator op) throws HibasOperatorException {
        if(op instanceof Honap){
            Honap ho = (Honap) op;
            int gy = ho.gy;
            int honap = ho.honap;

            return true;
        }else if(op instanceof Jatek){
            Jatek j = (Jatek) op;
            int gy = j.gy;
            int jatek = j.jatek;

            return true;
        }else if(op instanceof Torta){
            Torta t = (Torta) op;
            int gy = t.gy;
            int torta = t.torta;
            
            return true;
        }
        throw new HibasOperatorException();
    }

    @Override
    public Allapot alkalmaz(Operator op) throws HibasOperatorException {
        //potencionális hibák:
        // copy&paste esetén az indexek hibásak lehetnek. oszlop-sor indexet használunk!!!
        // lemarad az uj. ekkor az eredetit irkáljuk felül, ami hibát fog eredményezni
        if (op instanceof Honap) {
            Honap ho = (Honap) op;
            VidamAllapot uj = new VidamAllapot(this);
            //41-es feladatnál van mit kel ide írni
            uj.h[ho.gy][1] = ho.honap;
            return uj;
        } else if (op instanceof Jatek) {
            Jatek j = (Jatek) op;
            VidamAllapot uj = new VidamAllapot(this);
            uj.h[j.gy][2] = j.jatek;
            return uj;
        } else if (op instanceof Torta) {
            Torta t = (Torta) op;
            VidamAllapot uj = new VidamAllapot(this);
            uj.h[t.gy][3] = t.torta;
            return uj;
        }
        throw new HibasOperatorException();
    }
}

package vidampark;

import allapotter.Operator;

/**
 * Created by Erubo on 17/05/15.
 */
public class Torta extends Operator {
    static final int CSOKOLADE = 1, DIO = 2, KARAMELL = 3, MANDULA = 4, VANILIA = 5;
    int gy;
    int torta;

    public Torta(int gy, int torta) {
        this.gy = gy;
        this.torta = torta;
    }

    private String szoveg(int torta){
        switch (torta){
            case CSOKOLADE:
                return "csokolade";
            case DIO:
                return "dio";
            case KARAMELL:
                return "karamell";
            case MANDULA:
                return "mandula";
            case VANILIA:
                return "vanilia";
        }
        return null;
    }

    @Override
    public String toString() {
        return "Torta{" +
                "gy=" + gy +
                ", torta=" + szoveg(torta) +
                '}';
    }
}

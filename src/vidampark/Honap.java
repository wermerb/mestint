package vidampark;

import allapotter.Operator;

/**
 * Created by Erubo on 17/05/15.
 */
public class Honap extends Operator{

    int gy;
    int honap;

    public Honap(int gy, int honap) {
        this.gy = gy;
        this.honap = honap;
    }

    @Override
    public String toString() {
        return "Honap{" +
                "honap=" + honap +
                ", gy=" + gy +
                '}';
    }
}

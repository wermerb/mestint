package vidampark;

import allapotter.Operator;

/**
 * Created by Erubo on 17/05/15.
 */
public class Jatek extends Operator{

    static final int DODZSEM = 1, KASTELY = 2, HULLAMVASUT = 3, KORHINTA = 4, SZELLEMVASUT = 5;
    int gy;
    int jatek;

    public Jatek(int gy, int jatek) {
        this.gy = gy;
        this.jatek = jatek;
    }

    private String szoveg(int jatek){
        switch (jatek){
            case DODZSEM:
                return "dodzsem";
            case KASTELY:
                return "kastely";
            case HULLAMVASUT:
                return "hullamvasut";
            case KORHINTA:
                return "korhinta";
            case SZELLEMVASUT:
                return "szellemvasut";
        }
        return null;
    }
    @Override
    public String toString() {
        return "Jatek{" +
                "gy=" + gy +
                ", jatek=" + szoveg(jatek) +
                '}';
    }
}
